import hashlib
import re
from flask import Blueprint, make_response, session, request, redirect, url_for, jsonify
from module.recommend import *
from module.user import *
from module.game import *
from controller.user import user_disabled


recommend = Blueprint('recommend', __name__)


@recommend.route("/recommend/query", methods=['GET'])
def recommend_query():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    recommend = Recommend.get_by_user_id(u_id)
    if not recommend:
        return {"info": "not exist" ,"code": 404}

    return jsonify(recommend.to_json())


@recommend.route("/recommend/set", methods=['POST'])
def recommend_set():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    game_platform = request.json.get("game_platform")
    if game_platform:
        game_platform = game_platform.strip()
    
    game_level = request.json.get("game_level")
    if game_level:
        game_level = game_level.strip()
    
    game_duration = request.json.get("game_duration")
    if game_duration:
        game_duration = game_duration.strip()
    
    recommend = Recommend.get_by_user_id(u_id)
    if recommend:
        Recommend.modify(u_id, game_platform, game_level, game_duration)
    else:
        recommend = Recommend()
        recommend.user_id = u_id
        recommend.game_platform = game_platform
        recommend.game_level = game_level
        recommend.game_duration = game_duration
        recommend.add()
    return  {"info": "success" ,"code": 0}


@recommend.route("/recommend/delete", methods=['POST'])
def recommend_delete():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    Recommend.delete_by_user_id(u_id)
    return  {"info": "success" ,"code": 0}
