
import hashlib
import re
from flask import Blueprint, make_response, session, request, redirect, url_for, jsonify, current_app
from module.subscribe import *
from module.user import *
from module.game import *
from controller.user import user_disabled


subscribe = Blueprint('subscribe', __name__)


@subscribe.route("/subscribe/game", methods=['GET'])
def get_subscribe_by_username():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "user not login", "code": 1}

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    # subscribes = find_by_user_id(u_id)
    subscribe = Subscribe().get_by_user_id(u_id)
    
    platforms = set()
    if subscribe:
        if subscribe.game_platform:
            platform_strs = subscribe.game_platform.split(',')
            for platform_str in platform_strs:
                if subscribe.game_level:
                    game_level_strs = subscribe.game_level.split(',')
                    for game_level_str in game_level_strs:
                        if game_level_str:
                            platforms.add((platform_str.strip(), int(game_level_str)))
    current_app.logger.info('platforms:%s' % platforms)
    resp = []
    for platform, level in platforms:
        games = Game.get_by_platform_level(platform, level)
        for game in games:
            if game.cust_public == 0:
                if u_id != game.cust_user_id:
                    continue
            resp.append(game.to_json())
    return jsonify(resp)


@subscribe.route("/subscribe/id&game", methods=['GET'])
def get_subscribe_by_username_and_type():
    username = session.get('user_name')
    if not username:
        return {"info": "user not login", "code": 1}
    user = User().get_by_username(username).first()
    if not user:
        return {"info": "user not exist", "code": 1}
    u_id = user.user_id

    g_type = request.args.get('game_type')
    if g_type:
        g_type = g_type.strip()

    # subscribes = find_by_user_and_type(u_id, g_type)
    subscribes = Subscribe.get_by_user_and_type(u_id, g_type)
    resp = []
    for subscribe in subscribes:
        resp.append(subscribe.to_json())
    return jsonify(resp)


@subscribe.route("/subscribe/id&level", methods=['GET'])
def get_subscribe_by_username_and_level():
    username = session.get('user_name')
    if not username:
        return {"info": "user not login", "code": 1}
    
    user = User().get_by_username(username)
    if not user:
        return {"info": "user not exist", "code": 1}
    u_id = user.user_id

    level = request.args.get('level')
    if level:
        level = int(level)
    else:
        return {"info": "level is null", "code": 1}

    # subscribes = find_by_user_and_level(u_id, level)
    subscribes = Subscribe.get_by_user_and_level(u_id, level)
    platforms = set()
    for subscribe in subscribes:
        platform_strs = subscribe.platform.split(',')
        for platform_str in platform_strs:
            platforms.add((platform_str.strip(), subscribe.level_))
    resp = []
    for platform, level in platforms:
        games = Game.get_by_platform_level(platform, level)
        for game in games:
            resp.append(game.to_json())
    return jsonify(resp)


@subscribe.route("/subscribe/id&plat", methods=['GET'])
def get_subscribe_by_username_and_plat():
    username = session.get('user_name')
    if not username:
        return {"info": "user not login", "code": 1}
    
    user = User().get_by_username(username)
    if not user:
        return {"info": "user not exist", "code": 1}
    u_id = user.user_id

    plat = request.args.get('platform')
    if plat:
        plat = plat.strip()
    else:
        return {"info": "plat is null", "code": 1}
    # subscribes = find_by_user_and_plat(u_id, plat)
    subscribes = Subscribe.get_by_user_and_plat(u_id, plat)
    platforms = set()
    for subscribe in subscribes:
        platform_strs = subscribe.platform.split(',')
        for platform_str in platform_strs:
            platforms.add((platform_str.strip(), subscribe.level_))
    resp = []
    for platform, level in platforms:
        games = Game.get_by_platform_level(platform, level)
        for game in games:
            resp.append(game.to_json())
    return jsonify(resp)


@subscribe.route("/subscribe/update", methods=['POST'])
def update_subscribe():
    m_subscribe = Subscribe

    username = request.form.get("username").strip()
    u_id = dbsession.query(User).filter_by(user_name=username)
    dbsession.close()

    m_subscribe.user_id = u_id
    m_subscribe.game_type = request.form.get("game_type").strip()
    m_subscribe.level_ = request.form.get("level").strip()
    m_subscribe.platform = request.form.get("platform").strip()

    m_subscribe.update_subscribe()
    return


@subscribe.route("/subscribe/delete", methods=['GET'])
def delete_subscribe():
    m_subscribe = Subscribe

    username = request.form.get("username").strip()
    u_id = dbsession.query(User).filter_by(user_name=username)
    dbsession.close()

    m_subscribe.user_id = u_id
    m_subscribe.game_type = request.form.get("game_type").strip()
    m_subscribe.level_ = request.form.get("level").strip()
    m_subscribe.platform = request.form.get("platform").strip()

    m_subscribe.delete_subscribe()
    return


@subscribe.route("/subscribe/query", methods=['GET'])
def subscribe_query():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    subscribe = Subscribe().get_by_user_id(u_id)
    if not subscribe:
        return {"info": "not exist" ,"code": 404}

    return jsonify(subscribe.to_json())


@subscribe.route("/subscribe/set", methods=['POST'])
def subscribe_set():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    game_platform = request.json.get("game_platform")
    if game_platform:
        game_platform = game_platform.strip()
    
    game_level = request.json.get("game_level")
    if game_level:
        game_level = game_level.strip()
    
    game_duration = request.json.get("game_duration")
    if game_duration:
        game_duration = game_duration.strip()
    
    subscribe = Subscribe().get_by_user_id(u_id)
    if subscribe:
        Subscribe.modify(u_id, game_platform, game_level, game_duration)
    else:
        subscribe = Subscribe()
        subscribe.user_id = u_id
        subscribe.game_platform = game_platform
        subscribe.game_level = game_level
        subscribe.game_duration = game_duration
        subscribe.update_subscribe()
    return  {"info": "success" ,"code": 0}

@subscribe.route("/subscribe/delete", methods=['POST'])
def subscribe_delete():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    Subscribe.delete_by_user_id(u_id)
    return  {"info": "success" ,"code": 0}