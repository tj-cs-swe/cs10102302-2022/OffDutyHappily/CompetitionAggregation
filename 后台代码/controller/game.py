import hashlib
import re
from flask import Blueprint, make_response, session, request, redirect, url_for, jsonify, current_app
from module.game import *
from module.user import *
from module.recommend import *
from module.reserve import *
from module.subscribe import *
from common.utility import *
from controller.user import user_disabled
from controller.site_mail import site_mail_add
from datetime import datetime

game = Blueprint('game', __name__)


@game.route("/game/all", methods=['GET'])
def get_all_game():
    games = Game().get_all()
    resp = []
    for game in games:
        resp.append(game.to_json())
    return jsonify(resp)


@game.route("/game/all_by_time", methods=['GET'])
def get_all_game_by_time():
    mytime = request.args.get('game_start_time')
    if not mytime:
        return {"info": "game_start_time is null", "code": 1}

    # games = get_game_after_date(mytime)
    mytime = datetime.strptime(mytime, '%Y-%m-%d %H:%M:%S')
    games = Game().get_after_date(mytime)
    resp = []
    for game in games:
        resp.append(game.to_json())
    return jsonify(resp)


# 传回最新i个比赛的信息，用于主页展示，后续可以更改
@game.route('/displaygames', methods=['GET'])
def display_games():
    if request.method == 'GET':
        idx = request.args.get('index')
        if not idx:
            return {"info": "index is null", "code": 1}
        idx = int(idx)
        # game = get_all_game()[0-index]
        games = Game().get_all()
        resp = []
        num = 0
        for game in games:
            num += 1
            if num > idx:
                break
            resp.append(game.to_json())
        return jsonify(resp)
        # return {"name": game.game_name, "start": game.game_start_time.strftime('%Y-%m-%d %H:%M:%S'),
        #        "end": game.game_end_time.strftime('%Y-%m-%d %H:%M:%S'),
        #		    "duration": game.duration,"website": game.website,"type":game.game_type ,
        #        "level": game.level_, "platform": game.platform}
    return {}


@game.route("/game/add", methods=['POST'])
def game_add():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    game_name = request.json.get("game_name")
    if not game_name:
        return {"info": "game_name is null", "code": 500}
    if len(game_name) > 50:
        return {"info": "题目超长", "code": 501}
    game_name = game_name.strip()

    game_start_time = request.json.get("game_start_time")
    if not game_start_time:
        return {"info": "game_start_time is null", "code": 500}
    game_start_time = game_start_time.strip()
    game_start_time = datetime.strptime(game_start_time, '%Y-%m-%d %H:%M:%S')

    game_end_time = request.json.get("game_end_time")
    if game_end_time:
        game_end_time = game_end_time.strip()
        game_end_time = datetime.strptime(game_end_time, '%Y-%m-%d %H:%M:%S')

    duration = request.json.get("duration")
    if duration:
        duration = duration.strip()

    checked = 0

    website = request.json.get("website")
    if website:
        if len(website) > 100:
            return {"info": "网址超长", "code": 501}
        website = website.strip()
    else:
        website = ''

    game_type = request.json.get("game_type")
    if game_type:
        game_type = game_type.strip()
    else:
        game_type = ''

    level = request.json.get("level")
    if level:
        level = int(level)
    else:
        level = 0

    platform = request.json.get("platform")
    if platform:
        platform = platform.strip()
    else:
        platform = ''

    cust_reason = request.json.get("cust_reason")
    if cust_reason:
        cust_reason = cust_reason.strip()
    else:
        cust_reason = ''

    cust_public = request.json.get("cust_public")
    if cust_public:
        cust_public = int(cust_public)
    else:
        cust_public = 0

    game = Game()
    game.game_name = game_name
    game.game_start_time = game_start_time
    game.game_end_time = game_end_time
    game.duration = duration
    game.checked = checked
    game.website = website
    game.game_type = game_type
    game.level_ = level
    game.platform = platform
    game.cust_reason = cust_reason
    game.cust_user_id = u_id
    game.cust_public = cust_public
    game.add()
    return {"info": "success", "code": 0}


@game.route("/game/update", methods=['POST'])
def game_update():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    data = {}
    game_id = request.json.get("game_id")
    if not game_id:
        return {"info": "game_id is null", "code": 500}

    game_name = request.json.get("game_name")
    if not game_name:
        return {"info": "game_name is null", "code": 500}
    if len(game_name) > 50:
        return {"info": "题目超长", "code": 501}
    game_name = game_name.strip()
    data['game_name'] = game_name

    game_start_time = request.json.get("game_start_time")
    if not game_start_time:
        return {"info": "game_start_time is null", "code": 500}
    game_start_time = game_start_time.strip()
    game_start_time = datetime.strptime(game_start_time, '%Y-%m-%d %H:%M:%S')
    data['game_start_time'] = game_start_time

    game_end_time = request.json.get("game_end_time")
    if game_end_time:
        game_end_time = game_end_time.strip()
        game_end_time = datetime.strptime(game_end_time, '%Y-%m-%d %H:%M:%S')
    data['game_end_time'] = game_end_time

    duration = request.json.get("duration")
    if duration:
        duration = duration.strip()
    data['duration'] = duration

    data['checked'] = 0

    website = request.json.get("website")
    if website:
        if len(website) > 100:
            return {"info": "网址超长", "code": 501}
        website = website.strip()
    else:
        website = ''
    data['website'] = website

    game_type = request.json.get("game_type")
    if game_type:
        game_type = game_type.strip()
    else:
        game_type = ''
    data['game_type'] = game_type

    level = request.json.get("level")
    if level:
        level = int(level)
    else:
        level = 0
    data['level_'] = level

    platform = request.json.get("platform")
    if platform:
        platform = platform.strip()
    else:
        platform = ''
    data['platform'] = platform

    cust_reason = request.json.get("cust_reason")
    if cust_reason:
        cust_reason = cust_reason.strip()
    else:
        cust_reason = ''
    data['cust_reason'] = cust_reason

    data['cust_user_id'] = u_id

    cust_public = request.json.get("cust_public")
    if cust_public:
        cust_public = int(cust_public)
    else:
        cust_public = 0
    data['cust_public'] = cust_public

    try:
        Game.modify(game_id, data)
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "error", "code": 1}
    return {"info": "success", "code": 0}


@game.route("/game/delete", methods=['POST'])
def game_delete():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    game_id = request.json.get("game_id")
    if not game_id:
        return {"info": "game_id is null", "code": 500}

    game = Game.get_by_id(game_id)
    if not game:
        return {"info": "赛事不存在", "code": 501}
    cust_user_id = game.cust_user_id
    game_name = game.game_name

    Reserve.delete_by_game_id(game_id)

    Game.delete_by_game_id(game_id)

    user = User().get_by_id(u_id)
    if u_id != cust_user_id and user.user_type == 'admin':
        site_mail_add(cust_user_id, '您的赛事被管理员删除', '您好，您的帖子<<%s>>被管理员删除' % game_name)
    return {"info": "success", "code": 0}


@game.route("/game/by_start_time_range", methods=['GET'])
def get_all_game_by_start_end():
    start_time = request.args.get('start_time')
    if not start_time:
        return {"info": "start_time is null", "code": 500}
    start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')

    end_time = request.args.get('end_time')
    if not end_time:
        return {"info": "end_time is null", "code": 500}
    end_time = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')

    games = Game.by_start_time_range(start_time, end_time)
    resp = []
    for game in games:
        resp.append(game.to_json())
    return jsonify(resp)


@game.route("/game/query", methods=['GET'])
def game_query():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    start_time = request.args.get('start_time')
    if start_time:
        start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')

    end_time = request.args.get('end_time')
    if end_time:
        end_time = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')

    if start_time and end_time:
        games = Game.by_start_time_range(start_time, end_time)
    elif start_time:
        games = Game.get_after_date(start_time)
    elif end_time:
        games = Game.by_start_time_end(end_time)
    else:
        games = Game().get_all()

    recommend = Recommend.get_by_user_id(u_id)
    subscribe = Subscribe().get_by_user_id(u_id)

    resp = []
    for game in games:
        if game.cust_public == 0:
            if u_id != game.cust_user_id:
                current_app.logger.info('ignore %s' % game.game_id)
                continue

        g = {}
        g['game_id'] = game.game_id
        g['game_name'] = game.game_name
        if game.game_start_time:
            g['game_start_time'] = game.game_start_time.strftime('%Y-%m-%d %H:%M:%S')
        else:
            g['game_start_time'] = ''
        if game.game_end_time and type(game.game_end_time) != str:
            g['game_end_time'] = game.game_end_time.strftime('%Y-%m-%d %H:%M:%S')
        else:
            g['game_end_time'] = ''
        g['duration'] = game.duration
        g['game_name'] = game.game_name
        g['checked'] = game.checked
        g['website'] = game.website
        g['game_type'] = game.game_type
        g['level'] = game.level_
        g['platform'] = game.platform
        g['cust_reason'] = game.cust_reason
        g['cust_user_id'] = game.cust_user_id
        g['cust_public'] = game.cust_public

        game_duration_calculated = '0'
        if game.game_start_time and game.game_end_time:
            time_delta = game.game_end_time - game.game_start_time
            time_delta = time_delta.total_seconds()
            game_duration_calculated = get_duration(time_delta)

        g['reserved'] = False
        if Reserve().get_by_user_and_game(u_id, game.game_id):
            g['reserved'] = True

        g['recommended'] = False
        recommend_degree = 0
        if recommend:
            if recommend.game_level and str(game.level_) in recommend.game_level:
                g['recommended'] = True
                recommend_degree += 1
            if recommend.game_platform and game.platform and game.platform in recommend.game_platform:
                g['recommended'] = True
                recommend_degree += 1
            if recommend.game_duration and game_duration_calculated in recommend.game_duration:
                g['recommended'] = True
                recommend_degree += 1
        recommend_degree = round(recommend_degree / 3 * 100.0)
        g['recommend_degree'] = recommend_degree

        g['subscribed'] = False
        if subscribe:
            if subscribe.game_level and str(
                    game.level_) in subscribe.game_level and subscribe.game_platform and game.platform and game.platform in subscribe.game_platform:
                g['subscribed'] = True

        g['other'] = False
        if not g['reserved'] and not g['recommended'] and not g['subscribed']:
            g['other'] = True

        resp.append(g)
    return jsonify(resp)


@game.route("/game/custom/query/user", methods=['GET'])
def game_custom_query_user():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    resp = []
    games = Game.get_by_cust_user_id(u_id)
    for game in games:
        resp.append(game.to_json())
    return jsonify(resp)


@game.route("/game/custom/query/all", methods=['GET'])
def game_custom_query_all():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    resp = []
    games = Game.get_by_cust()
    for game in games:
        resp.append(game.to_json())
    return jsonify(resp)


@game.route("/game/check", methods=['POST'])
def game_check():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    game_id = request.json.get("game_id")
    if not game_id:
        return {"info": "game_id is null", "code": 500}

    game = Game.get_by_id(game_id)
    if not game:
        return {"info": "赛事不存在", "code": 501}

    data = {}
    checked = request.json.get("checked")
    if not checked:
        return {"info": "checked is null", "code": 500}
    checked = int(checked)
    if checked not in (1, 2):
        return {"info": "审核状态非法", "code": 500}

    data['checked'] = checked

    cust_reason = request.json.get("cust_reason")
    if cust_reason:
        cust_reason = cust_reason.strip()
    else:
        cust_reason = ''
    data['cust_reason'] = cust_reason

    Game.modify(game_id, data)

    if checked == 1:
        mail_headline = '恭喜您，您上传的赛事已经审核通过'
        mail_content = '恭喜您，您上传的赛事《%s》已经审核通过' % game.game_name
    elif checked == 2:
        mail_headline = '对不起，您的赛事审核不通过'
        mail_content = '对不起，您的赛事《%s》审核不通过，理由是%s' % (game.game_name, cust_reason)
    site_mail_add(game.cust_user_id, mail_headline, mail_content)

    return {"info": "success", "code": 0}
