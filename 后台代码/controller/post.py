import hashlib
import re
from flask import Blueprint, make_response, session, request, redirect, url_for, jsonify, current_app
from sqlalchemy.exc import IntegrityError
from common.utility import *
from module.post import *
from module.comment import *
from module.post_statistic import PostStatistic
from module.comment_statistic import CommentStatistic
from controller.site_mail import site_mail_add
from controller.user import user_disabled

from datetime import datetime

post = Blueprint('post', __name__)


@post.route('/post/add', methods=['POST'])
def post_add():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    headline = request.json.get("headline")
    if not headline:
        return {"info": "标题为空", "code": 501}
    headline = headline.strip()
    if len(headline) > 80:
        return {"info": "标题超长", "code": 500}

    content = request.json.get("content")
    if not content:
        return {"info": "内容为空", "code": 501}

    if len(content) > 4000:
        return {"info": "内容超长", "code": 501}
    content = content.strip()

    drafted = request.json.get("drafted")
    if drafted is None:
        drafted = 0

    checked = 1

    block_type = request.json.get("block_type")
    if block_type is None:
        return {"info": "block_type is null", "code": 500}

    topped = 0

    com_count = 0

    post = Post()
    post.user_id = u_id
    post.headline = headline
    post.content = content
    post.drafted = drafted
    post.checked = checked
    post.block_type = block_type
    post.topped = topped
    post.com_count = com_count
    post.add()
    return {"info": "success", "code": 0}


@post.route('/post/update', methods=['POST'])
def post_update():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    post_id = request.json.get("post_id")
    if post_id is None:
        return {"info": "post_id is null", "code": 500}

    headline = request.json.get("headline")
    if not headline:
        return {"info": "标题为空", "code": 501}
    headline = headline.strip()
    if len(headline) > 80:
        return {"info": "标题超长", "code": 500}

    content = request.json.get("content")
    if not content:
        return {"info": "内容为空", "code": 501}

    if len(content) > 4000:
        return {"info": "内容超长", "code": 501}
    content = content.strip()

    drafted = request.json.get("drafted")
    if drafted is None:
        drafted = 0

    checked = 1

    block_type = request.json.get("block_type")
    if block_type is None:
        return {"info": "block_type is null", "code": 500}

    topped = 0

    com_count = 0

    data = {}
    data['headline'] = headline
    data['content'] = content
    data['drafted'] = drafted
    data['checked'] = checked
    data['block_type'] = block_type
    data['topped'] = topped
    data['com_count'] = com_count
    data['update_time'] = datetime.now()
    Post.modify(post_id, data)
    return {"info": "success", "code": 0}


@post.route("/post/delete", methods=['POST'])
def post_delete():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    post_id = request.json.get("post_id")
    if not post_id:
        return {"info": "post_id is null", "code": 500}

    post = Post().get_by_id(post_id)
    if not post:
        return {"info": "帖子不存在", "code": 501}

    PostStatistic.delete_by_post_id(post_id)
    CommentStatistic.delete_by_post_id(post_id)
    Comment.delete_by_post_id(post_id)
    Post.delete_by_post_id(post_id)

    user = User().get_by_id(u_id)
    if u_id != post.user_id and user.user_type == 'admin':
        site_mail_add(post.user_id, '您的帖子被管理员删除', '您好，您的帖子<<%s>>被管理员删除' % post.headline)
    return {"info": "success", "code": 0}


# 具体阅读某一篇的文章
@post.route('/post/<int:post_id>')
def post_read(post_id):
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    post = Post().get_by_id(post_id)
    if not post:
        return {"info": "帖子不存在", "code": 501}
    user = User().get_by_id(post.user_id)
    if not user:
        return {"info": "帖子的用户不存在", "code": 501}

    data = post.to_json()
    data.update({'user_name': user.user_name})
    return jsonify(data)


# 用于显示板块的文章列表
@post.route('/block/<int:block_type>', methods=['GET'])
def read_type(block_type):
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    page_num = request.args.get("page_num")
    if page_num is None:
        page_num = 1
    else:
        page_num = int(page_num)
    page_size = request.args.get("page_size")
    if page_size is None:
        page_size = 10
    else:
        page_size = int(page_size)
    resp = []
    posts = Post.get_limit_with_user_block(block_type, page_num, page_size)
    for post, user in posts.items:
        data = post.to_json()
        data.update({'user_name': user.user_name})
        resp.append(data)
    return jsonify(resp)


# 获取热度高的文章
@post.route('/hot')
def get_hot():
    post = Post().get_by_hot()
    return jsonify(model_join_list(post))


@post.route("/post/top", methods=['POST'])
def post_top():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    user = User().get_by_id(u_id)
    if user.user_type != 'admin':
        return {"info": "user has no permission", "code": 500}

    post_id = request.json.get("post_id")
    if not post_id:
        return {"info": "post_id is null", "code": 500}
    Post.set_post_topped(post_id)
    return {"info": "success", "code": 0}


@post.route("/post/top/cancel", methods=['POST'])
def post_top_cancel():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    user = User().get_by_id(u_id)
    if user.user_type != 'admin':
        return {"info": "user has no permission", "code": 500}

    post_id = request.json.get("post_id")
    if not post_id:
        return {"info": "post_id is null", "code": 500}
    Post.cancel_post_topped(post_id)
    return {"info": "success", "code": 0}


@post.route('/post/agree', methods=['POST'])
def post_agree():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    post_id = request.json.get('post_id')

    statistic = PostStatistic()
    statistic.user_id = u_id
    statistic.post_id = post_id
    statistic.statistic_type = 0
    try:
        statistic.add()
    except IntegrityError as e:
        return {"info": "您已经点过赞了", "code": 501}
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "error", "code": 500}

    try:
        Post.post_agree(post_id)
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "error", "code": 500}  # 新增失败
    return {"info": "success", "code": 0}


@post.route('/post/oppo', methods=['POST'])
def post_oppo():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录

    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

    post_id = request.json.get('post_id')

    statistic = PostStatistic()
    statistic.user_id = u_id
    statistic.post_id = post_id
    statistic.statistic_type = 1
    try:
        statistic.add()
    except IntegrityError as e:
        return {"info": "您已经反对过了", "code": 501}
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "error", "code": 500}

    try:
        Post.post_oppo(post_id)
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "error", "code": 500}  # 新增失败
    return {"info": "success", "code": 0}
