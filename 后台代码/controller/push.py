import hashlib
import re
from flask import Blueprint, make_response, session, request, redirect, url_for, jsonify
from module.push import *
from module.user import *
from module.game import *
from controller.user import user_disabled


push = Blueprint('push', __name__)


@push.route("/push/query", methods=['GET'])
def push_query():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    push_info = Push.get_by_user_id(u_id)
    if not push_info:
        return {"info": "not exist" ,"code": 404}

    return jsonify(push_info.to_json())


@push.route("/push/set", methods=['POST'])
def push_set():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    push_time = request.json.get("push_time")
    if push_time:
        push_time = push_time.strip()
    
    push_method = request.json.get("push_method")
    if push_method:
        push_method = push_method.strip()
    
    game_platform = request.json.get("game_platform")
    if game_platform:
        game_platform = game_platform.strip()
    
    game_level = request.json.get("game_level")
    if game_level:
        game_level = game_level.strip()
    
    game_duration = request.json.get("game_duration")
    if game_duration:
        game_duration = game_duration.strip()

    push = Push.get_by_user_id(u_id)
    if push:
        Push.modify(u_id, push_time, push_method, game_platform, game_level, game_duration)
    else:
        push = Push()
        push.user_id = u_id
        push.push_time = push_time
        push.push_method = push_method
        push.game_platform = game_platform
        push.game_level = game_level
        push.game_duration = game_duration
        push.add()
    return  {"info": "success" ,"code": 0}


@push.route("/push/delete", methods=['POST'])
def push_delete():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    Push.delete_by_user_id(u_id)
    return  {"info": "success" ,"code": 0}
