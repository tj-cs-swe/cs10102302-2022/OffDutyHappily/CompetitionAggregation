from flask import current_app
from sqlalchemy import Table
from common.database import dbconnect

import time

dbsession, md, DBase = dbconnect()


class User(DBase):
    __table__ = Table('user', md, autoload=True)

    # 以下为自定义的方法
    def add(self):
        try:
            dbsession.add(self)
            dbsession.commit()
        finally:
            dbsession.close()

    def rm(self):
        try:
            dbsession.delete(self)
            dbsession.commit()
        finally:
            dbsession.close()

    def get_by_username(self, name):
        try:
            res = dbsession.query(User).filter_by(user_name=name).first()
            current_app.logger.info('get user by user name:%d' % name)
            return res
        finally:
            dbsession.close()

    def get_by_id(self, id):
        try:
            res = dbsession.query(User).filter_by(user_id=id).first()
            current_app.logger.info('get user by user id:%d' % id)
            return res
        finally:
            dbsession.close()

    def get_by_phone(self, phone):
        try:
            res = dbsession.query(User).filter_by(phone=phone).first()
            current_app.logger.info('get user by user phone:%d' % phone)
            return res
        finally:
            dbsession.close()

    def get_by_mail(self, mail):
        try:
            res = dbsession.query(User).filter_by(mail=mail).first()
            current_app.logger.info('get user by user mail:%d' % mail)
            return res
        finally:
            dbsession.close()

    def modify_info(self, user_id, user_name, mail, preferrence, phone):
        data = {}

        if user_name != None:
            data['user_name'] = user_name

        if mail != None:
            data['mail'] = mail

        if preferrence != None:
            data['preferrence'] = preferrence

        if phone != None:
            data['phone'] = phone

        try:
            dbsession.query(User).filter_by(user_id=user_id).update(data)
            current_app.logger.info('modify user info by user id:%d' % user_id)
            dbsession.commit()
            return {"info": "success", "code": 0}
        except:
            return {"info": "error", "code": 1}
        finally:
            dbsession.close()

    @classmethod
    def modify_password(cls, user_name, password):
        data = {}
        data['password'] = password

        try:
            dbsession.query(User).filter_by(user_name=user_name).update(data)
            current_app.logger.info('modify user password by user name:%d' % user_name)
            dbsession.commit()
        finally:
            dbsession.close()

    @classmethod
    def modify_black(cls, user_id, black):
        data = {}
        data['black'] = black

        try:
            dbsession.query(User).filter_by(user_id=user_id).update(data)
            current_app.logger.info('modify user black by user id:%d' % user_id)
            dbsession.commit()
        finally:
            dbsession.close()

    @classmethod
    def query(cls):
        try:
            res = dbsession.query(User).all()
            current_app.logger.info('query all users')
            return res
        finally:
            dbsession.close()

    # 以下内容只有管理员才有权限操作
    # 禁用该用户的权限
    def set_user_black(self, user_id):
        data = {'black': 1}
        try:
            dbsession.query(User).filter_by(user_id=user_id).update(data)
            current_app.logger.info('set user black by user id:%d' % user_id)
            dbsession.commit()
            # dbsession.close()
            return {"info": "success", "code": 0}
        except:
            return {"info": "error", "code": 1}
        finally:
            dbsession.close()

    # 恢复该用户的权限
    def set_user_white(self, user_id):
        data = {'black': 0}
        try:
            dbsession.query(User).filter_by(user_id=user_id).update(data)
            current_app.logger.info('set user white by user id:%d' % user_id)
            dbsession.commit()
            # dbsession.close()
            return {"info": "success", "code": 0}
        except:
            return {"info": "error", "code": 1}
        finally:
            dbsession.close()

    def to_json(self):
        resp = {}
        for k, v in self.__dict__.items():
            if not k.startswith('_sa'):
                if k == 'password':
                    continue
                resp[k] = v
        return resp
