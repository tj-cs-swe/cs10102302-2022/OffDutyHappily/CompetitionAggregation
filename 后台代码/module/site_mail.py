from flask import current_app
from sqlalchemy import Table
from common.database import dbconnect

from datetime import datetime

dbsession, md, DBase = dbconnect()


class SiteMail(DBase):
    __table__ = Table('site_mail', md, autoload=True)

    # 以下为自定义的方法
    def add(self):
        try:
            dbsession.add(self)
            dbsession.commit()
        finally:
            dbsession.close()

    @classmethod
    def get_by_user_id(cls, user_id, page_num, page_size):
        try:
            res = dbsession.query(SiteMail).filter_by(user_id=user_id).order_by(SiteMail.mail_id.desc()) \
                .paginate(page_num, per_page=page_size)
            current_app.logger.info('get site mail by user id:%d' % user_id)
            return res
        finally:
            dbsession.close()

    @classmethod
    def get_by_id(cls, mail_id):
        try:
            res = dbsession.query(SiteMail).filter_by(mail_id=mail_id).first()
            current_app.logger.info('get site mail by mail id:%d' % id)
            return res
        finally:
            dbsession.close()

    @classmethod
    def delete_by_id(cls, mail_id):
        try:
            dbsession.query(SiteMail).filter_by(mail_id=mail_id).delete()
            current_app.logger.info('delete site mail by mail id:%d' % id)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    @classmethod
    def status_update(cls, mail_id, mail_status):
        try:
            dbsession.query(SiteMail).filter_by(mail_id=mail_id).update({'mail_status': mail_status})
            current_app.logger.info('update site mail by mail id:%d' % mail_id)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    @classmethod
    def status_update_user_all(cls, user_id, mail_status):
        try:
            dbsession.query(SiteMail).filter_by(user_id=user_id).update({'mail_status': mail_status})
            current_app.logger.info('update site mail by user id:%d' % user_id)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    def to_json(self):
        resp = {}
        for k, v in self.__dict__.items():
            if not k.startswith('_sa'):
                if type(v) == datetime:
                    resp[k] = v.strftime('%Y-%m-%d %H:%M:%S')
                else:
                    resp[k] = v
        return resp
