from flask import current_app
from sqlalchemy import Table
from common.database import dbconnect

import time

dbsession, md, DBase = dbconnect()


class Recommend(DBase):
    __table__ = Table('recommend', md, autoload=True)

    # 以下为自定义的方法
    def add(self):
        try:
            dbsession.add(self)
            dbsession.commit()
        finally:
            dbsession.close()

    @classmethod
    def delete_by_user_id(cls, user_id):
        try:
            dbsession.query(Recommend).filter_by(user_id=user_id).delete()
            current_app.logger.info('delete recommend by user id :%d' % user_id)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    @classmethod
    def get_by_user_id(cls, user_id):
        try:
            res = dbsession.query(Recommend).filter_by(user_id=user_id).first()
            current_app.logger.info('get recommend by user id :%d' % user_id)
            return res
        finally:
            dbsession.close()

    @classmethod
    def modify(cls, user_id, game_platform, game_level, game_duration):
        data = {}

        if game_platform:
            data['game_platform'] = game_platform

        if game_level:
            data['game_level'] = game_level

        if game_duration:
            data['game_duration'] = game_duration

        try:
            dbsession.query(Recommend).filter_by(user_id=user_id).update(data)
            current_app.logger.info('modify recommend by user id :%d' % user_id)
            dbsession.commit()
            return {"info": "success", "code": 0}
        except:
            return {"info": "error", "code": 1}
        finally:
            dbsession.close()

    def to_json(self):
        resp = {}
        for k, v in self.__dict__.items():
            if not k.startswith('_sa'):
                resp[k] = v
        return resp
