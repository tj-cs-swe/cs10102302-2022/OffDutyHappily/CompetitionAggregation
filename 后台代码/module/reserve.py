from flask import current_app
from sqlalchemy import Table, Integer, Column
from common.database import dbconnect
import time

dbsession, md, DBase = dbconnect()


class Reserve(DBase):
    __table__ = Table('reserve', md, autoload=True)

    # reserve_id = Column(Integer, primary_key=True)
    # user_id = Column(Integer)
    # game_id = Column(Integer)

    # 以下为自定义的方法

    def update_reserve(self):
        try:
            dbsession.add(self)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    def delete_reserve(self):
        try:
            dbsession.query(Reserve).filter_by(reserve_id=self.reserve_id).delete()
            current_app.logger.info('delete reserve by reserve id :%d' % self.reserve_id)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    def get_by_id(self, reserve_id):
        try:
            reserve = dbsession.query(Reserve).filter_by(reserve_id=reserve_id).all()
            current_app.logger.info('get reserve by reverse id :%d' % reserve_id)
            return reserve
        finally:
            dbsession.close()

    def get_by_user(self, user_id):
        try:
            reserve = dbsession.query(Reserve).filter_by(user_id=user_id).all()
            current_app.logger.info('get reserve by user id :%d' % user_id)
            return reserve
        finally:
            dbsession.close()

    def get_by_user_and_game(self, user_id, game_id):
        try:
            reserve = dbsession.query(Reserve).filter_by(user_id=user_id, game_id=game_id).all()
            current_app.logger.info('get reserve by user id:%d and game id:%d' % (user_id, game_id))
            return reserve
        finally:
            dbsession.close()

    @classmethod
    def delete_by_reserve_id(cls, reserve_id):
        try:
            dbsession.query(Reserve).filter_by(reserve_id=reserve_id).delete()
            current_app.logger.info('delete reserve by reserve id :%d' % reserve_id)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    @classmethod
    def delete_by_user_id(cls, user_id):
        try:
            dbsession.query(Reserve).filter_by(user_id=user_id).delete()
            current_app.logger.info('delete reserve by user id :%d' % user_id)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    @classmethod
    def delete_by_game_id(cls, game_id):
        try:
            dbsession.query(Reserve).filter_by(game_id=game_id).delete()
            current_app.logger.info('delete reserve by game id :%d' % game_id)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    @classmethod
    def delete_by_user_id_game_id(cls, user_id, game_id):
        try:
            dbsession.query(Reserve).filter_by(user_id=user_id, game_id=game_id).delete()
            current_app.logger.info('delete reserve by user id:%d and game id:%d' % (user_id, game_id))
            dbsession.commit()
            return
        finally:
            dbsession.close()

    def to_json(self):
        resp = {}
        for k, v in self.__dict__.items():
            if not k.startswith('_sa'):
                resp[k] = v
        return resp
