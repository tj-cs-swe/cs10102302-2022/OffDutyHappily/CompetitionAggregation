from flask import current_app
from sqlalchemy import Table
from common.database import dbconnect

import time

dbsession, md, DBase = dbconnect()


class CommentStatistic(DBase):
    __table__ = Table('comment_statistic', md, autoload=True)

    # 以下为自定义的方法
    def add(self):
        try:
            dbsession.add(self)
            dbsession.commit()
        finally:
            dbsession.close()

    @classmethod
    def delete_by_post_id(cls, post_id):
        try:
            dbsession.query(CommentStatistic).filter_by(post_id=post_id).delete()
            current_app.logger.info('delete comment by post id:%d all' % post_id)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    @classmethod
    def delete_by_comment_id(cls, comment_id):
        try:
            dbsession.query(CommentStatistic).filter_by(comment_id=comment_id).delete()

            current_app.logger.info('delete comment by comment id:%d all' % comment_id)
            dbsession.commit()
            return
        finally:
            dbsession.close()
