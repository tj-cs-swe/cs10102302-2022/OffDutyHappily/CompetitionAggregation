from flask import session, request, current_app
from sqlalchemy import Table
from common.database import dbconnect
from common.utility import model_join_list
from module.user import User
import time
from datetime import datetime

# from common.utility import model_join_list

dbsession, md, DBase = dbconnect()


class Comment(DBase):
    __table__ = Table('comment', md, autoload=True)

    # 新增一条评论
    @classmethod
    def insert_comment(cls, user_id, post_id, content):
        now = time.strftime('%Y-%m-%d %H:%M:%S')

        comment = Comment(user_id=user_id, post_id=post_id,
                          content=content, create_time=now, reply_id=0)
        current_app.logger.info('add comment to db')
        dbsession.add(comment)
        dbsession.commit()
        dbsession.close()

    @classmethod
    def get_by_id(cls, comment_id):
        result = dbsession.query(Comment).filter_by(comment_id=comment_id).first()
        current_app.logger.info('get comment from db by id:%d' % comment_id)
        dbsession.close()
        return result

    @classmethod
    def get_by_reply_id(cls, reply_id):
        result = dbsession.query(Comment).filter_by(reply_id=reply_id).all()
        current_app.logger.info('get comment from db by reply id:%d' % reply_id)
        dbsession.close()
        return result

    @classmethod
    def get_by_sub_reply_id(cls, sub_reply_id):
        result = dbsession.query(Comment).filter_by(sub_reply_id=sub_reply_id).all()
        current_app.logger.info('get comment from db by sub reply id:%d' % sub_reply_id)
        dbsession.close()
        return result

    # 根据文章编号查看所有评论
    def get_by_postid(self, post_id):
        result = dbsession.query(Comment).filter_by(post_id=post_id, hidden=0, reply_id=0).all()
        current_app.logger.info('get comment from db by post id:%d' % post_id)
        dbsession.close()
        return result

    @classmethod
    def insert_reply(cls, user_id, post_id, comment_id, reply_id, content):
        now = time.strftime('%Y-%m-%d %H:%M:%S')
        comment = Comment(user_id=user_id, post_id=post_id,
                          content=content, reply_id=comment_id, sub_reply_id=reply_id,
                          create_time=now)
        current_app.logger.info('insert reply to db :%s' % content)
        dbsession.add(comment)
        dbsession.commit()
        dbsession.close()

        # 查找评论与用户信息，注意评论区也要分页

    def get_limit_with_user(self, post_id, start, count):
        res = dbsession.query(Comment, User).join(User, User.user_id == Comment.user_id) \
            .filter(Comment.post_id == post_id, Comment.hidden == 0) \
            .order_by(Comment.comment_id.desc()).limit(count).offset(start).all()
        current_app.logger.info('get user:%s limit' % User.user_name)
        dbsession.close()
        return res

    def get_comment_with_user(self, post_id, start, count):
        res = dbsession.query(Comment, User).join(User, User.user_id == Comment.user_id) \
            .filter(Comment.post_id == post_id, Comment.hidden == 0, Comment.reply_id == 0) \
            .order_by(Comment.comment_id.desc()).limit(count).offset(start).all()
        current_app.logger.info('get user:%s comments' % User.user_name)
        dbsession.close()
        return res

    def get_reply_with_user(self, reply_id):
        res = dbsession.query(Comment, User.user_name).join(User, User.user_id == Comment.user_id) \
            .filter(Comment.hidden == 0, Comment.reply_id == reply_id).all()
        current_app.logger.info('get user:%s replies' % User.user_name)
        dbsession.close()
        return res

    def get_comment_user_list(self, post_id, start, count):
        res1 = self.get_comment_with_user(post_id, start, count)
        comment_list = model_join_list(res1)

        # for comment in comment_list:
        #     res = self.get_reply_with_user(comment['comment_id'])
        #     comment['reply_list'] = model_join_list(res)
        list = []
        list += comment_list

        while list:
            layer = []
            for i in list:
                res2 = self.get_reply_with_user(i['comment_id'])
                i['reply_list'] = model_join_list(res2)
                layer += i['reply_list']

            list.clear()
            list += layer

        print(comment_list[1])
        return comment_list

    @classmethod
    def get_comment_list_with_user(cls, post_id, page_num, page_size):
        res = dbsession.query(Comment, User).join(User, User.user_id == Comment.user_id) \
            .filter(Comment.post_id == post_id, Comment.hidden == 0, Comment.reply_id == 0) \
            .order_by(Comment.comment_id.desc()).paginate(page_num, per_page=page_size)
        current_app.logger.info('get user:%s all comments' % User.user_name)
        dbsession.close()
        return res

    @classmethod
    def get_reply_list_with_user(cls, reply_id):
        res = dbsession.query(Comment, User).join(User, User.user_id == Comment.user_id) \
            .filter(Comment.hidden == 0, Comment.reply_id == reply_id) \
            .order_by(Comment.comment_id.desc()).all()
        current_app.logger.info('get user:%s all replies' % User.user_name)
        dbsession.close()
        return res

    def get_count_by_postid(self, post_id):
        count = dbsession.query(Comment).filter_by(post_id=post_id, hidden=0, reply_id=0).count()
        current_app.logger.info('get count by post id:%d' % post_id)
        dbsession.close()
        return count

    @classmethod
    def comment_agree(cls, comment_id):
        res = dbsession.query(Comment).filter_by(comment_id=comment_id).update(
            {Comment.agree_count: Comment.agree_count + 1})
        current_app.logger.info('agree comment by comment id:%d' % comment_id)
        dbsession.commit()
        dbsession.close()
        return res

    @classmethod
    def comment_oppo(cls, comment_id):
        res = dbsession.query(Comment).filter_by(comment_id=comment_id).update(
            {Comment.oppo_count: Comment.oppo_count + 1})
        dbsession.commit()
        dbsession.close()
        return res

    @classmethod
    def delete_by_post_id(cls, post_id):
        dbsession.query(Comment).filter_by(post_id=post_id).delete()
        current_app.logger.info('delete comment by post id:%d all' % post_id)
        dbsession.commit()
        dbsession.close()
        return

    @classmethod
    def delete_by_comment_id(cls, comment_id):
        dbsession.query(Comment).filter_by(comment_id=comment_id).delete()
        current_app.logger.info('delete comment by comment id:%d all' % comment_id)
        dbsession.commit()
        dbsession.close()
        return

    def to_json(self):
        resp = {}
        for k, v in self.__dict__.items():
            if not k.startswith('_sa'):
                if type(v) == datetime:
                    resp[k] = v.strftime('%Y-%m-%d %H:%M:%S')
                else:
                    resp[k] = v
        return resp
