from flask import current_app
from sqlalchemy import Table
from common.database import dbconnect

import time

dbsession,md,DBase=dbconnect()


class PostStatistic(DBase):
    __table__ = Table('post_statistic', md, autoload=True)
    #以下为自定义的方法
    def add(self):
        try:
            dbsession.add(self)
            dbsession.commit()
        finally:
            dbsession.close()
    
    @classmethod
    def delete_by_post_id(cls, post_id):
        try:
            dbsession.query(PostStatistic).filter_by(post_id = post_id).delete()
            current_app.logger.info('delete post by post id :%d' % post_id)
            dbsession.commit()
            return
        finally:
            dbsession.close()
