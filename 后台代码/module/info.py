from flask import current_app
from sqlalchemy import Table, func
from common.database import dbconnect
import time
from module.user import *

dbsession, md, DBase = dbconnect()


class Info(DBase):
    __table__ = Table('info', md, autoload=True)

    def get_info_by_user(self, user_id):
        try:
            res = dbsession.query(Info).filter(Info.user_id == user_id).all()
            current_app.logger.info('get info by user id :%d' % user_id)
            return res
        finally:
            dbsession.close()

    def add(self):
        try:
            dbsession.add(self)
            dbsession.commit()
        finally:
            dbsession.close()

    def rm(self):
        try:
            dbsession.delete(self)
            dbsession.commit()
        finally:
            dbsession.close()
