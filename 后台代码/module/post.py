from flask import current_app
from sqlalchemy import Table, func
from common.database import dbconnect
import time
from datetime import datetime
from module.user import User

dbsession, md, DBase = dbconnect()


class Post(DBase):
    __table__ = Table('post', md, autoload=True)

    def add(self):
        dbsession.add(self)
        dbsession.commit()
        dbsession.close()

    @classmethod
    def modify(cls, post_id, data):
        dbsession.query(Post).filter_by(post_id=post_id).update(data)
        current_app.logger.info('modify post by post id :%d' % post_id)
        dbsession.commit()
        dbsession.close()

    @classmethod
    def delete_by_post_id(cls, post_id):
        dbsession.query(Post).filter_by(post_id=post_id).delete()
        current_app.logger.info('delete post by post id :%d' % post_id)
        dbsession.commit()
        dbsession.close()
        return

    # 以下为自定义的方法
    # 查询所有文章
    def get_all(self):
        res = dbsession.query(Post).all()
        current_app.logger.info('get all posts')
        dbsession.close()
        return res

    # 根据id查询文章
    def get_by_id(self, post_id):
        row = dbsession.query(Post).filter(Post.post_id == post_id, Post.drafted == 0, Post.checked == 1).first()
        current_app.logger.info('get post by post id :%d' % post_id)
        dbsession.close()
        return row

    # 指定分页的limit和offset，同时与User表作连接查询,实现分页功能
    def get_limit_with_user(self, start, count):
        # 倒叙排列
        res = dbsession.query(Post, User.user_name).join(User, User.user_id == Post.user_id) \
            .filter(Post.drafted == 0, Post.checked == 1).order_by(Post.post_id.desc()) \
            .limit(count).offset(start).all()
        current_app.logger.info('get posts by limit:%d and offset:%d' % (count, start))
        dbsession.close()
        return res

    @classmethod
    def get_limit_with_user_block(cls, block_type, page_num, page_size):
        # 倒叙排列
        res = dbsession.query(Post, User).join(User, User.user_id == Post.user_id) \
            .filter(Post.drafted == 0, Post.checked == 1, Post.block_type == block_type).order_by(
            Post.topped.desc()).order_by(Post.update_time.desc()) \
            .paginate(page_num, per_page=page_size)
        current_app.logger.info('get post limit by user:%d and block:%s' % (Post.user_id, block_type))
        dbsession.close()
        return res

    def get_limit_with_block(self, block_type, page_num, page_size):
        # 倒叙排列
        res = dbsession.query(Post) \
            .filter(Post.drafted == 0, Post.checked == 1, Post.block_type == block_type).order_by(
            Post.topped.desc()).order_by(Post.update_time.desc()) \
            .paginate(page_num, per_page=page_size)
        current_app.logger.info('get post limit by block:%s' % block_type)
        dbsession.close()
        return res

    def get_by_hot(self):
        max = dbsession.query(func.max(Post.com_count)).all()
        avg = dbsession.query(func.avg(Post.com_count)).all()

        res = dbsession.query(Post, User.user_name).join(User, User.user_id == Post.user_id).filter(
            Post.com_count >= avg, Post.com_count <= max).all()
        current_app.logger.info('get post by hot')
        dbsession.close()
        return res

    # 文章置顶
    @classmethod
    def set_post_topped(cls, post_id):
        data = {'topped': 1, 'update_time': datetime.now()}
        dbsession.query(Post).filter_by(post_id=post_id).update(data)
        current_app.logger.info('set post top by post id :%d' % post_id)
        dbsession.commit()
        dbsession.close()

    # 取消文章置顶
    @classmethod
    def cancel_post_topped(cls, post_id):
        data = {'topped': 0, 'update_time': datetime.now()}
        dbsession.query(Post).filter_by(post_id=post_id).update(data)
        current_app.logger.info('cancel post top by post id :%d' % post_id)
        dbsession.commit()
        dbsession.close()

    # 审核赛事
    def set_post_checked(self, post_id):
        data = {'checked': 1}
        try:
            dbsession.query(Post).filter_by(post_id=post_id).update(data)
            current_app.logger.info('check post by post id :%d' % post_id)
            dbsession.commit()
            dbsession.close()
            return {"info": "success", "code": 0}
        except:
            return {"info": "error", "code": 1}

    @classmethod
    def post_agree(cls, post_id):
        res = dbsession.query(Post).filter_by(post_id=post_id).update({Post.agree_count: Post.agree_count + 1})
        current_app.logger.info('agree post top by post id :%d' % post_id)
        dbsession.commit()
        dbsession.close()
        return res

    @classmethod
    def post_oppo(cls, post_id):
        res = dbsession.query(Post).filter_by(post_id=post_id).update({Post.oppo_count: Post.oppo_count + 1})
        current_app.logger.info('oppo post top by post id :%d' % post_id)
        dbsession.commit()
        dbsession.close()
        return res

    def to_json(self):
        resp = {}
        for k, v in self.__dict__.items():
            if not k.startswith('_sa'):
                if type(v) == datetime:
                    resp[k] = v.strftime('%Y-%m-%d %H:%M:%S')
                else:
                    resp[k] = v
        return resp
