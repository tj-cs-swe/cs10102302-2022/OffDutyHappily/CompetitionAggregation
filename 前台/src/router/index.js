import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Layout from '../views/Layout'
import AdminLayout from '../views/Layout/adminLayout'
import store from "@/store";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: '当前位置',
        component: Layout,
        redirect: '/calendar',
        children: [
            {
                path: 'home',
                name: 'home',
                meta: {title: 'Let\'s code - 算法竞赛开赛提醒聚合站',},
                component: Home
            },
            {
                path: 'calendar',
                name: 'calendar',
                meta: {title: 'Let\'s code - 算法竞赛开赛提醒聚合站',},
                component: () => import('../views/Calendar.vue')
            },
            {
                path: 'subscribe',
                name: 'subscribe',
                meta: {title: 'Let\'s code - 算法竞赛开赛提醒聚合站',},
                component: () => import('../views/Subscribe.vue')
            },
            {
                path: 'discussion',
                name: 'discussion',
                meta: {title: 'Let\'s code - 算法竞赛开赛提醒聚合站',},
                component: () => import('../views/Discussion.vue')
            },
            {
                path: 'postDetail',
                name: 'postDetail',
                meta: {title: 'Let\'s code - 算法竞赛开赛提醒聚合站',},
                component: () => import('../views/PostDetail')
            },
            {
                path: 'user',
                name: 'user',
                meta: {title: '个人主页 - Let\'s code - 算法竞赛开赛提醒聚合站',},
                component: () => import('../views/User/Index.vue')
            },
            {
                path: 'notice',
                name: 'notice',
                meta: {title: '站内通知 - Let\'s code - 算法竞赛开赛提醒聚合站',},
                component: () => import('../views/notice')
            }
        ]
    },
    {
        path: '/admin',
        name: '后台管理',
        component: AdminLayout,
        redirect: '/admin/userManger',
        children: [
            {
                path: 'userManger',
                name: 'userManger',
                meta: {title: '后台管理 - 用户管理 - Let\'s code - 算法竞赛开赛提醒聚合站',},
                component: () => import('../views/Admin/userManger')
            },{
                path: 'gameManger',
                name: 'gameManger',
                meta: {title: '后台管理 - 自定义赛事管理 - Let\'s code - 算法竞赛开赛提醒聚合站',},
                component: () => import('../views/Admin/gameManger')
            },
        ]
    },
    {
        path: '/register',
        name: 'Register',
        meta: {title: '注册 - Let\'s code - 算法竞赛开赛提醒聚合站',},
        component: () => import('../views/Register.vue')
    },
    {
        path: '/login',
        name: 'Login',
        meta: {title: '登录 - Let\'s code - 算法竞赛开赛提醒聚合站',},
        component: () => import('../views/Login.vue')
    },
    {
        path: '/forgetPassword',
        name: 'ForgetPassword',
        meta: {title: '重置密码 - Let\'s code - 算法竞赛开赛提醒聚合站',},
        component: () => import('../views/ForgetPassword')
    },
    {
        path: '/register/reset_password',
        name: 'ResetPassword',
        meta: {title: '重设密码 - Let\'s code - 算法竞赛开赛提醒聚合站',},
        component: () => import('../views/reset_password.vue')
    },
    {
        path: '/test',
        name: 'Test',
        meta: {title: '功能测试页面 - Let\'s code - 算法竞赛开赛提醒聚合站',},
        component: () => import('../views/Test.vue')
    },
    {
        path: '/404',
        name: '404',
        meta: {title: '乌乌，页面丢失了 - Let\'s code - 算法竞赛开赛提醒聚合站',},
        component: () => import('../views/404.vue')
    },
    {
        path: '*',    // 此处需特别注意至于最底部
        redirect: '/404'
    }
]

const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
    const token = sessionStorage.getItem('token');
    let validateArr = ['Login', 'Register', 'ForgetPassword', 'ResetPassword', '404']
    if (validateArr.includes(to.name)) {
        if (token) {
            next('/')
            return
        }
        if (sessionStorage.exitStatus === '1') {
            Vue.prototype.$notification.warn({message: '登录已过期，已强制退出'})
        } else if (sessionStorage.exitStatus === '2') {
            Vue.prototype.$notification.warn({message: '您的账户被封禁，已强制退出'})
        }
        sessionStorage.clear()
        next()
        return
    }
    if (!token) {
        next('/login')
        return
    }
    store.state.user.user_info.user_name = sessionStorage.user_name
    if (!store.state.user.user_info.user_id) {
        store.dispatch('user/getUserInfo')
    }
    next()
})

export default router
