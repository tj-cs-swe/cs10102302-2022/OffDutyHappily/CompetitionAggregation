import axios from 'axios'
import store from "@/store";

// 创建axios实例
const service = axios.create({
    baseURL: process.env.VUE_APP_BASE_API, // api 的 base_url
    timeout: 60000, // request timeout
})

service.defaults.withCredentials = true

// 请求拦截
service.interceptors.request.use(
    config => {
        return config
    },
    error => {
        // 请求发生错误处理
        console.log('request' + error) // for debug
        Promise.reject(error)
    }
)

// 响应拦截
service.interceptors.response.use(
    response => {
        console.log(response)
        let filterUrl = [
            'login', 'register/user/check', 'register/email/check', 'register/phone/check',
            'register', 'register/reset_password'
        ]
        if (response.data.code == 1 && !filterUrl.includes(response.config.url)) {
            store.dispatch('common/layout', '1')
        } else if (response.data.code == 502) {
            store.dispatch('common/layout', '2')
        }
        return response.data
    }, error => {
        // 响应发生错误处理
        console.error('error==>',  error.response)// for debug
        return Promise.reject(error)
    })

export default service
