export default {
    namespaced: true,
    state: {
        routerName: ['home']
    },

    actions: {
        layout({state}, params) {
            if (sessionStorage['noticeTimer']) {
                clearTimeout(sessionStorage['noticeTimer'])
            }
            sessionStorage.clear()
            if (params) {
               sessionStorage.exitStatus = params
            }
            location.reload()
        }
    }
}