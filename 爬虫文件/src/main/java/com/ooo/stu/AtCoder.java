package com.ooo.stu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AtCoder {
    public static List<Game> findGame(){
        List<Game> games = new ArrayList<>();
        try {
            Document document = Jsoup.connect("https://atcoder.jp/contests/").get();
            System.out.println(document.body());
            Elements elements = document.select("div#contest-table-upcoming tbody tr");
            for(Element tr : elements){
                String name = tr.select("td").get(1).select("a").text();
                String time = tr.select("time.fixtime").text();
                String duration = tr.select("td").get(2).text();
                String url = tr.select("td").get(1).select("a").attr("href");
                String hours = duration.split(":")[0];
                String minutes = duration.split(":")[1];

                Calendar c = Calendar.getInstance();
                try {
                    c.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(time));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Date startTime = c.getTime();
                c.add(Calendar.HOUR,Integer.parseInt(hours));
                c.add(Calendar.MINUTE,Integer.parseInt(minutes));
                Date endTime = c.getTime();

                String id = url.split("/")[2];
                String href = "https://atcoder.jp" + url;

                String group = null;

                Game game = new Game();
                game.setTitle(StringUtils.substring(name,0,50));
                game.setStartDate(startTime);
                game.setEndDate(endTime);
                game.setDuration(duration);
                game.setLink(href);
                game.setGroup(group);
                game.setOid("atcoder-" + id);
                game.setPlatform("atcoder");
                game.setDifficulty("1");
                games.add(game);
            }
            for(Game g: games) {
                System.out.println(g);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return games;
    }
}
