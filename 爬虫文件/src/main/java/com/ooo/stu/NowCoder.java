package com.ooo.stu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NowCoder {
    public static List<Game> findGame(){
        List<Game> games = new ArrayList<>();
        try {
            Document document = Jsoup.connect("https://ac.nowcoder.com/acm/contest/vip-end-index?topCategoryFilter=13").get();
            System.out.println(document.body());
            Elements elements = document.select("div.platform-item");
            for(Element tr : elements){
                String json = tr.attr("data-json");
                json = StringEscapeUtils.unescapeHtml(json);
                JSONObject jsonObject = JSON.parseObject(json);
                String name = jsonObject.getString("contestName");
                String oid = "nowcoder" + jsonObject.getString("contestId");
                Date startTime = new Date(jsonObject.getLong("contestStartTime"));
                Date endTime = new Date(jsonObject.getLong("contestEndTime"));

                String duration = TimeUtils.getDatePoor(startTime,endTime);

                String href = tr.select("div.platform-item-cont h4 a").attr("href");
                href = "https://ac.nowcoder.com" + href;

                String group = null;
                Elements gl = tr.select("div.platform-item-cont ul li.icon-nc-flash2");
                if(gl != null) {
                    group = gl.text();
                }

                Game game = new Game();
                game.setTitle(name);
                game.setStartDate(startTime);
                game.setEndDate(endTime);
                game.setDuration(duration);
                game.setLink(href);
                game.setGroup(group);
                game.setOid(oid);
                game.setPlatform("牛客网");
                games.add(game);
            }
            for(Game g: games) {
                System.out.println(g);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return games;
    }
}
